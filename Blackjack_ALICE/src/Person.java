
public abstract class Person {
	private Hand hand;

	private boolean bust, stood;

	public Person() {
		bust = false;
		stood = false;
		hand = new Hand();
	}

	public void drawCard(Deck d) {
		hand.addCard(d.drawCard());
		if (getHand().getTotalValue() > 21)
			bust = true;
	}

	public void resetHand() {
		bust = stood = false;
		hand.resetHand();
	}

	public Hand getHand() {
		return hand;
	}

	public void setHand(Hand h) {
		hand = h;
	}

	public boolean hasBust() {
		return bust;
	}

	public boolean hasStood() {
		return stood;
	}

	public void setBust() {
		bust = true;
	}

	public void setStood() {
		stood = true;
	}
	
	public abstract void win(double amt);
	public abstract void bet(double wager);
}
