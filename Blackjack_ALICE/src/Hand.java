import java.util.ArrayList;

public class Hand {

	private ArrayList<Card> hand;
	private int cardTotal;

	public Hand() {
		resetHand();
	}

	public void addCard(Card c) {
		hand.add(c);
		cardTotal = calculateTotal();
	}

	private int calculateTotal() {
		int total = 0;

		for (int i = 0; i < hand.size(); i++) {
			RANK cardRank = hand.get(i).getRank();
			total += cardRank.getValue();
		}

		// if ace value can be 11, add 10 (1 was already added earlier)
		if (containsAce() && total + 10 <= 21)
			total += 10;
		return total;
	}

	public boolean containsAce() {
		for (Card c : hand)
			if (c.getRank() == RANK.ACE)
				return true;
		return false;
	}

	public ArrayList<Card> cards() {
		return hand;
	}

	public int getTotalValue() {
		return cardTotal;
	}

	public void resetHand() {
		hand = new ArrayList<Card>();
		cardTotal = 0;
	}

}
