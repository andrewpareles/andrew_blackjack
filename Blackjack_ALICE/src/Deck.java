import java.util.ArrayList;

public class Deck {
	private ArrayList<Card> deck = new ArrayList<Card>();

	public Deck() { // start out with full deck
		resetDeck();
	}

	public void resetDeck() {// fill deck with all 52 cards
		deck = new ArrayList<Card>();
		for (int i = 0; i <= 3; i++)
			for (int j = 0; j <= 12; j++)
				deck.add(new Card(RANK.values()[j], SUIT.values()[i]));
	}

	public ArrayList<Card> getDeck() {
		return deck;
	}

	public Card drawCard() {// pick random card from deck and remove it
		return deck.remove((int) (Math.random() * deck.size()));
	}

	public void putCard(Card c) {// put a card back in the deck
		deck.add(c);
	}

}
