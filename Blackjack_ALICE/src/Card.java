
public class Card {
	private RANK rank;
	private SUIT suit;

	public Card(RANK rank, SUIT suit) {
		this.rank = rank;
		this.suit = suit;
	}

	public SUIT getSuit() {
		return suit;
	}

	public RANK getRank() {
		return rank;
	}

}
