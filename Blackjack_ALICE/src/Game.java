import java.util.ArrayList;
import java.util.Scanner;

public class Game {
	static final int pbet = 10;
	static final int dbet = 10;
	static final int startmoney = 100;

	Deck deck;
	int roundNum;
	Player[] players;
	Dealer dealer;

	public Game(int numPlayers) {
		deck = new Deck();
		roundNum = 0;
		players = new Player[numPlayers];
		dealer = new Dealer();

		for (int i = 0; i < numPlayers; i++)
			players[i] = new Player(i, startmoney);
	}

	public static void main(String[] args) {
		Game game = new Game(2);
		game.play();

	}

	public void play() {
		System.out.println("HOUSE RULES:\nYou can only split (once) on the first draw, "
				+ "double down on the first draw, and surrender on the first draw.\n\n");

		endRoundAndStartNext();
		printRoundHeader();
		// round number starts at 1, so make it 0 (start on p1)
		int turnNum = roundNum - 1;

		printTable(false);

		boolean atLeastOnePlayerIn = true;

		while (atLeastOnePlayerIn) {

			// player's turn
			if (players[turnNum].isIn()) {
				move(players[turnNum]);
			} else {
				printOutOfMoney(players[turnNum]);
			}
			turnNum = (turnNum + 1) % players.length; // loop around

			if (readyForNextRound()) {
				move(dealer);

				printTable(true);

				ArrayList<Person> winners = determineRoundWinners();
				int winMoney = giveWinnings(winners);
				printWinnings(winners, winMoney);

				endRoundAndStartNext();
				turnNum = (roundNum - 1) % players.length;

				printRoundHeader();
				printTable(false);

				atLeastOnePlayerIn = false;
				for (Player p : players)
					if (p.isIn()) {
						atLeastOnePlayerIn = true;
						break;
					}
			}

		}

	}

	private boolean readyForNextRound() {
		// next round checks
		int numStoodOrSurrendered = 0;
		for (Player p : players) {
			// p1.hasStood && p2.hasStood && ...
			if (p.hasSurrendered() || p.hasStood() || !p.isIn())
				numStoodOrSurrendered++;
		}
		return numStoodOrSurrendered == players.length;
	}

	public int calculateTotalPot() {
		int total = dbet;
		for (Player p : players)
			total += p.getCurrentBet();
		return total;
	}

	private void endRoundAndStartNext() {
		// deal cards, increment round number, place bets and see if no money
		roundNum++;
		deck.resetDeck();

		// reset hands and bet
		// dealer
		dealer.resetHand();
		dealer.drawCard(deck);
		dealer.drawCard(deck);
		dealer.bet(dbet);

		// players
		for (Player p : players) {
			p.resetHand();

			if (p.getBalance() >= pbet) {
				p.bet(pbet);
			} else {
				p.dropOut();
			}

			if (p.isIn()) {
				p.drawCard(deck);
				p.drawCard(deck);
			}
		}
	}

	private void printRoundHeader() {
		System.out.println("\n####################");
		System.out.println("***NEW ROUND***");
		System.out.println("Hand number " + roundNum + ":");

		System.out.println("Dealer's wager: $" + dbet + ". Total earnings: $" + dealer.getTotalProfit() + ".");
		for (int i = 0; i < players.length; i++)
			System.out.println("Player " + players[i].getPlayerNum() + " wager: $" + players[i].getCurrentBet()
					+ ". Current balance: $" + players[i].getBalance() + ".");
		System.out.println("Total table wager: $" + calculateTotalPot() + ".");
		System.out.println("####################");
	}

	private void printOutOfMoney(Player p) {
		System.out.println("Player " + p.getPlayerNum() + " is out of money.");
	}

	private void printTable(boolean allFaceUp) {
		// allFaceUp is true at end of game
		System.out.println("-------------------------------------");
		System.out.println("***CURRENT TABLE***");
		System.out.println("Total table wager: $" + calculateTotalPot() + ".");
		System.out.print("Dealer's hand: ");

		if (allFaceUp)
			printHand(dealer.getHand());
		else {
			printCard(dealer.getHand().cards().get(0));
			int numHiddenCards = (dealer.getHand().cards().size() - 1);
			// I could have just had one hidden card since dealer moves last,
			// but this is the generalization...
			System.out.println(" | (" + numHiddenCards + " hidden card" + (numHiddenCards > 1 ? "s" : "") + ")");
		}

		for (int i = 0; i < players.length; i++) {
			Player p = players[i];
			boolean hasSplit = p.getSplitHand().cards().size() != 0;

			System.out.print("Player " + p.getPlayerNum() + "'s hand: ");
			if (p.hasSurrendered())
				System.out.print("(SURRENDERED) | ");
			else if (p.hasBust())
				System.out.print("(BUST) | ");
			else if (hasSplit)
				System.out.print("(SPLIT) | ");
			else if (p.hasDoubledDown())
				System.out.print("(DOUBLED DOWN) | ");
			else if (p.hasStood())
				System.out.print("(STOOD) | ");
			else {
				// should never be reached...
			}

			// if split hand is worth more than normal hand
			if (p.getSplitHand().getTotalValue() > p.getHand().getTotalValue())
				printHand(p.getSplitHand());
			else
				printHand(p.getHand());
		}
		System.out.println("-------------------------------------");

	}

	private void printWinnings(ArrayList<Person> winners, int amt) {
		System.out.println("\n********** Winners ********** ");
		for (Person p : winners) {
			if (p instanceof Player)
				System.out.print("Player " + ((Player) p).getPlayerNum());
			else if (p instanceof Dealer)
				System.out.print("Dealer");
			System.out.print(" | ");
		}
		System.out.println("$" + amt + (winners.size() > 1 ? " each" : ""));

	}

	private void printHand(Hand h) {

		for (int i = 0; i < h.cards().size(); i++) {
			printCard(h.cards().get(i));
			// if (i != h.cards().size() - 1)
			System.out.print(" | ");
		}
		System.out.println("(" + h.getTotalValue() + ")");

	}

	public void printCard(Card c) {

		// prints card in format "<RANK> of <SUIT>"
		String s = c.getRank().name() + " of " + c.getSuit().name();
		System.out.print(s);
	}

	private ArrayList<Person> determineRoundWinners() {
		ArrayList<Person> winners = new ArrayList<Person>();
		int highHand = dealer.hasBust() ? 0 : dealer.getHand().getTotalValue();
		if (!dealer.hasBust())
			winners.add(dealer);

		for (Player p : players) {
			int handTotal = p.getHand().getTotalValue();
			int splitTotal = p.getSplitHand().getTotalValue();
			int total = Math.max(handTotal, splitTotal);

			// if new high, clear winners
			if (total > highHand && !p.hasBust()) {
				winners.clear();
				winners.add(p);
				highHand = total;
			}
			// tie
			else if (total == highHand) {
				winners.add(p);
			}
		}

		return winners;
	}

	private int giveWinnings(ArrayList<Person> winners) {
		int pot = calculateTotalPot();
		// if surrendered, return half of bet to player
		for (Player p : players)
			if (p.hasSurrendered()) {
				pot -= p.getCurrentBet() / 2;
				p.win(p.getCurrentBet() / 2);
			}

		// dealer wins if no one wins (dealer busts last...)
		if (winners.size() == 0) {
			dealer.win(pot);
			return pot;
		}

		// split winnings among winners
		int splitPot = pot / winners.size();
		if (splitPot == 0)
			splitPot = pot;

		for (Person p : winners)
			p.win(splitPot);

		return splitPot;
	}

	private void move(Person person) {
		System.out.println();
		// dealer move:
		if (person instanceof Dealer) {
			dealer.move(deck);
			System.out.println("Dealer has taken their turn.\n");
			return;
		}

		// If not a Dealer, then a Player, so can cast:
		Player p = (Player) person;

		p.setStood();

		// player move:
		Scanner scan = new Scanner(System.in);
		String s = "";

		System.out.print("Player " + p.getPlayerNum() + "'s current hand: ");
		printHand(p.getHand());

		// "s" ends the loop
		while (true) {
			s = "";
			// blackjack:
			if (p.getHand().getTotalValue() == 21) {
				System.out.println("Player " + p.getPlayerNum() + " has 21 and must stand.");
				printFinalHand(p);
				return;
			}
			// over 21:
			else if (p.hasBust()) {
				System.out.println("Bust! Player " + p.getPlayerNum() + " is over 21.");
				printFinalHand(p);
				return;
			}
			// under 21:
			else {
				while (s.equals("")) {
					// keep looping until something valid is written
					// print options as appropriate
					s = "";

					System.out.println("Player " + p.getPlayerNum() + "'s choices:\n"
							+ (p.getSplitHand().cards().size() == 0 ? "(H) Hit | " : "") + "(S) Stand "
							+ (p.isOnFirstCards() ? "| (D) Double down " : "")
							+ ((p.isOnFirstCards()
									&& p.getHand().cards().get(0).getRank() == p.getHand().cards().get(1).getRank())
											? "| (SP) Split " : "")
							+ (p.isOnFirstCards() ? "| (SU) Surrender" : ""));
					s = scan.nextLine();
					s = s.toLowerCase();

					// HIT
					if (p.getSplitHand().cards().size() == 0 && (s.equals("h") || s.equals("hit"))) {
						p.drawCard(deck);
						System.out.print("Player " + p.getPlayerNum() + "'s current hand: ");
						printHand(p.getHand());

					}

					// STAND
					else if (s.equals("s") || s.equals("stand")) {// nop
						printFinalHand(p);
						return;
					}

					// DOUBLE DOWN
					else if (p.isOnFirstCards() && (s.equals("d") || s.equals("double down"))) {
						// double down only on round 1
						if (p.getBalance() >= pbet) {
							p.drawCard(deck);
							p.bet(pbet);

							p.doubleDown();

							System.out.println(
									"Player " + p.getPlayerNum() + " has doubled down. Additional bet: $" + pbet + ".");
							printFinalHand(p);
							return;
						}
						// not enough money so redo
						else {
							printOutOfMoney(p);
							s = "";
						}
					}

					// SPLIT
					else if (p.isOnFirstCards()
							&& p.getHand().cards().get(0).getRank() == p.getHand().cards().get(1).getRank()
							&& (s.equals("sp") || s.equals("split"))) {
						// if 1st turn and 1st card rank equals second card rank
						if (p.getBalance() >= pbet) {

							p.getSplitHand().addCard(p.getHand().cards().remove(1));
							// move 2nd card of pHand into pHand2 (split hand)

							p.drawCard(deck);// draw again
							p.drawCardSplit(deck);

							p.bet(pbet);

							System.out.println(
									"Player " + p.getPlayerNum() + " has split. Additional bet: $" + pbet + ".");
							printFinalHand(p);
							return;
						} else {
							printOutOfMoney(p);
							s = "";// redo if not enough money to re-bet
						}
					}

					// SURRENDER
					else if (p.isOnFirstCards() && s.equals("su") || s.equals("surrender")) {
						p.surrender();
						return;
					} else {
						s = "";// redo loop if invalid
						System.out.println("Invalid entry.");
					}
					// if invalid still might just have two cards
					if (!s.equals(""))
						p.move();
				}

			}
		}
	}

	private void printFinalHand(Player p) {
		System.out.print("Player " + p.getPlayerNum() + "'s final hand: ");
		printHand(p.getHand());

		if (p.getSplitHand().cards().size() != 0) {
			System.out.print("Player " + p.getPlayerNum() + "'s split hand: ");
			printHand(p.getSplitHand());
		}

	}

}
