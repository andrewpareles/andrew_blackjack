
public class Dealer extends Person {

	private double totalProfit;

	public Dealer() {
		super();
		totalProfit = 0;
	}

	public void move(Deck d) {
		// if below 16, always hit
		// OR if soft 17, also hit (ace and any 6)
		while (getHand().getTotalValue() <= 16 || (getHand().containsAce() && getHand().getTotalValue() == 17))
			drawCard(d);
		// otherwise, if hard 17+, stand
		setStood();

	}

	public void win(double amt) {
		//round 2 decimal places
		totalProfit += Math.round(amt*100)/100;
	}

	public void bet(double wager) {
		totalProfit -= Math.round(wager*100)/100;
	}
	
	public double getTotalProfit(){
		return totalProfit;
	}
}
