
public class Player extends Person {

	private Hand splitHand;
	private boolean surrendered, doubled, onFirstCards, out;

	// pnum is player ordinal + 1 (no 0)
	private int pnum, currentBet;
	private double money;

	public Player(int pnum, int money) {
		super();

		this.pnum = pnum;
		this.money = money;

		surrendered = false;
		doubled = false;

		setHand(new Hand());
		splitHand = new Hand();

		onFirstCards = true;

		currentBet = 0;
		out = false;
	}

	public void dropOut() {
		out = true;
	}

	public boolean isIn() {
		return !out;
	}

	public void drawCardSplit(Deck d) {
		splitHand.addCard(d.drawCard());
	}

	public void move() {
		onFirstCards = false;
	}

	public Hand getSplitHand() {
		return splitHand;
	}

	public int getPlayerNum() {
		return pnum + 1;
	}

	public boolean hasSurrendered() {
		return surrendered;
	}

	public void surrender() {
		surrendered = true;
	}

	public boolean hasDoubledDown() {
		return doubled;
	}

	public void doubleDown() {
		doubled = true;
	}

	public void resetHand() {
		super.resetHand();
		splitHand.resetHand();
		surrendered = false;
		doubled = false;
		onFirstCards = true;
		currentBet = 0;
	}

	public double getBalance() {
		return money;
	}

	public void bet(double wager) { // bet and remove from money
		wager = Math.round(wager*100)/100;
		money -= wager;
		currentBet += wager;
	}

	public void win(double amt) { // add back the amount of money
		money += Math.round(amt*100)/100;
	}

	public boolean isOnFirstCards() {
		return onFirstCards;
	}

	public int getCurrentBet() {
		return currentBet;
	}

}
