
public enum RANK {
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;

	// note: returns 1 for ace
	int getValue() {
		int o = ordinal() + 1;
		// 1=ACE, 2=2, ... 11 = JACK, 12 = QUEEN, 13 = KING
		return o>10?10:o;
	}
}
